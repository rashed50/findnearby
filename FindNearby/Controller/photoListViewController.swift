//
//  photoListViewController.swift
//  Find Nearby
//
//  Created by Developer on 12/9/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class photoListViewController: UIViewController {
    var imageList = [UIImage]()

    var img:String?
    var currentImage = 0
    @IBOutlet weak var imgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: Selector(("respondToSwipeGesture:")))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
               self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: Selector(("respondToSwipeGesture:")))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
               self.view.addGestureRecognizer(swipeLeft)
        
        imgView.image = imageList[0]
        

        
    }
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {


            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.left:
                if currentImage == imageList.count - 1 {
                    currentImage = 0

                }else{
                    currentImage += 1
                }
                imgView.image = imageList[currentImage]

            case UISwipeGestureRecognizer.Direction.right:
                if currentImage == 0 {
                    currentImage = imageList.count - 1
                }else{
                    currentImage -= 1
                }
                imgView.image = imageList[currentImage]
            default:
                break
            }
        }
    }
    

    

}
