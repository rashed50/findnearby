//
//  MapViewController.swift
//  Feed Me
//
/// Copyright (c) 2017 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    var item = ["A","B","C"]
    var placesArray: [GooglePlace] = []
    @IBOutlet weak var tableView: UIView!
    
    var searchingCatagoryName : String?
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet private weak var mapCenterPinImage: UIImageView!
    @IBOutlet private weak var pinImageVerticalConstraint: NSLayoutConstraint!
    var searchedTypes = ["restaurant"]
    //"bakery", "bar", "cafe", "grocery_or_supermarket", "restaurant"
    private let locationManager = CLLocationManager()
    private let dataProvider = GoogleDataProvider()
    private let searchRadius: Double = 10000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.isHidden = true
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        searchedTypes = [searchingCatagoryName!]
        fetchNearbyPlaces(coordinate: mapView.camera.target)
        
        
        mapView.delegate = self
        
        tableViewOutlet.register(UINib(nibName: "mapListViewCell", bundle: nil), forCellReuseIdentifier: "mapListViewCell")
    }
    
    
    //  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    guard let navigationController = segue.destination as? UINavigationController,
    //      let controller = navigationController.topViewController as? TypesTableViewController else {
    //        return
    //    }
    //    controller.selectedTypes = searchedTypes
    //    controller.delegate = self
    //  }
    //
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            
            self.addressLabel.unlock()
            
            guard let address = response?.firstResult(), let lines = address.lines else {
                
                return
            }
            
            self.addressLabel.text = lines.joined(separator: "\n")
            
            let labelHeight = self.addressLabel.intrinsicContentSize.height
            self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                bottom: labelHeight, right: 0)
            //
            //      UIView.animate(withDuration: 0.25) {
            //        self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
            //        self.view.layoutIfNeeded()
            //      }
        }
    }
    
    func fetchNearbyPlaces(coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        
        dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes) { places in
            places.forEach {
                let marker = PlaceMarker(place: $0)
                marker.map = self.mapView
            }
            
            self.placesArray = places
        }
    }
    
    @IBAction func refreshPlaces(_ sender: Any) {
        fetchNearbyPlaces(coordinate: mapView.camera.target)
    }
    
    @IBAction func listButtonPressed(_ sender: UIBarButtonItem) {
        
        
        tableView.isHidden = !tableView.isHidden
        tableViewOutlet.reloadData()
        
        
    }
    
    func openDetailsView(obj :GooglePlace){
        
        
        
        if #available(iOS 13.0, *) {
            if let vc = storyboard?.instantiateViewController(identifier: "detailsViewController") as? DetailsViewController{
                vc.placeDetailsInfo.append(obj)
                navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "detailsViewController") as? DetailsViewController{
                
                vc.placeDetailsInfo.append(obj)
                
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
}

// MARK: - TypesTableViewControllerDelegate
//extension MapViewController: TypesTableViewControllerDelegate {
//  func typesController(_ controller: TypesTableViewController, didSelectTypes types: [String]) {
//    searchedTypes = controller.selectedTypes.sorted()
//    dismiss(animated: true)
//    fetchNearbyPlaces(coordinate: mapView.camera.target)
//  }
//}

// MARK: - CLLocationManagerDelegate
extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
        fetchNearbyPlaces(coordinate: location.coordinate)
    }
     

}
// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        addressLabel.lock()
        
        if (gesture) {
            mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let placeMarker = marker as? PlaceMarker else {
            return nil
        }
        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
            return nil
        }
        
        infoView.nameLabel.text = placeMarker.place.name
        if let photo = placeMarker.place.photo {
            infoView.placePhoto.image = photo
        } else {
            infoView.placePhoto.image = UIImage(named: "generic")
        }
        
        return infoView
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
    
    
    
    
    
    
}


extension MapViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mapListViewCell", for: indexPath) as! mapListViewCell
        
        cell.imgView.image = placesArray[indexPath.row].photo
        cell.nameLabel.text = placesArray[indexPath.row].name
        
        cell.addressLabel.text = placesArray[indexPath.row].address
        cell.ratingLabel.text = placesArray[indexPath.row].userRatings
        // cell.textLabel?.text = placesArray[indexPath.row].name
        
        let latitude = placesArray[indexPath.row].coordinate.latitude
        let longitude = placesArray[indexPath.row].coordinate.longitude
        
        placesArray[indexPath.row].distance = distanceCalculate(lat: latitude, lon:longitude , myLocation: mapView!.myLocation!)
        cell.distanceLabel.text = "Distance: \(placesArray[indexPath.row].distance)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openDetailsView(obj: placesArray[indexPath.row])
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func distanceCalculate(lat:Double, lon: Double, myLocation: CLLocation) -> String {
        var finalDistance:String
        let startLocation : CLLocation = CLLocation.init(latitude: lat, longitude: lon)
        let distance = startLocation.distance(from:myLocation )  as Double
        if distance > 1000 {
            let accurateDistance = distance / 1000
            finalDistance = String(format: "%.2f km", accurateDistance)
        }else {
            finalDistance = String(format: "%.0f m", distance)
        }
        return finalDistance
    }
    
    
    
}
