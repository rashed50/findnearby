//
//  AppDelegate.swift
//  Find Nearby
//
//  Created by Developer on 4/9/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import GoogleMaps


let googleApiKey = "AIzaSyBpJk4co82objew0ze_cfaJPnzDIC1byjI"
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    
    let containerViewController = ContainerViewController()
    
    window!.rootViewController = containerViewController
    window!.makeKeyAndVisible()
    GMSServices.provideAPIKey(googleApiKey)
    return true
  }
}
