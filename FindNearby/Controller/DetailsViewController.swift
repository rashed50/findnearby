//
//  DetailsViewController.swift
//  Find Nearby
//
//  Created by Developer on 11/9/20.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    var placeDetailsInfo = [GooglePlace]()
    var placeName:String?
    var distance:String?
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
  private let dataProvider = GoogleDataProvider()
    override func viewDidLoad() {
        super.viewDidLoad()

        placeNameLabel.text = placeDetailsInfo[0].name
        distanceLabel.text = placeDetailsInfo[0].distance
        
        dataProvider.fetchPlacesDetailsInformation(placeDetailsInfo[0].placeId) { places in
                  places.forEach {
                    print(($0))
                  }
                  
                   
              }
        
        
    }
    
    @IBAction func dialCallButton(_ sender: UIButton) {
        dialNumber(number: "01760011279")

    }
    
    func dialNumber(number : String) {

     if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
          if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
           } else {
               UIApplication.shared.openURL(url)
           }
       } else {
                // add error message here
       }
    }
    
    @IBAction func searchWebsiteURL(_ sender: UIButton) {
        
       var query = placeDetailsInfo[0].name
        query = query.replacingOccurrences(of: " ", with: "+")
        let url = "https://www.google.co.in/search?q=" + query
        UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
    }
    @IBAction func photoOpenButtonPressed(_ sender: UIButton) {
        
        let photo = placeDetailsInfo[0].photo
        if #available(iOS 13.0, *) {
                  if let vc = storyboard?.instantiateViewController(identifier: "photoListViewController") as? photoListViewController{
                    
//                    if let photo = placeMarker.place.photo {
//                         infoView.placePhoto.image = photo
//                       }
                    vc.imageList.append(photo!)
                      navigationController?.pushViewController(vc, animated: true)
                  }
              } else {
                   if let vc = storyboard?.instantiateViewController(withIdentifier: "photoListViewController") as? photoListViewController{
                                 
                    vc.imageList.append(photo!)
                      navigationController?.pushViewController(vc, animated: true)
                             }
              }
    }
    
    

}
