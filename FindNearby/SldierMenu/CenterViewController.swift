/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit

 

class CenterViewController: UIViewController {
    var nameString = Helper.getSearchTypeList()
        
 
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var creatorLabel: UILabel!
    
    @IBOutlet weak var textLabel: UILabel!
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    var cellIdentifier = "ItemCollectionViewCell"
    @IBOutlet weak var collectionView: UICollectionView!
    var delegate: CenterViewControllerDelegate?
    
    // MARK: Button actions
    @IBAction func kittiesTapped(_ sender: Any) {
        delegate?.toggleLeftPanel()
    }
    
    @IBAction func puppiesTapped(_ sender: Any) {
        // delegate?.toggleRightPanel()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupCollectionViewItemSize()
    }
    
    
    private func setupCollectionViewItemSize(){
        if collectionViewFlowLayout == nil {
            let numberOfItemPerRow: CGFloat = 4
            let lineSpacing: CGFloat = 5
            let interItemSpacing: CGFloat = 5
            
            let width =
                (collectionView.frame.width - ((numberOfItemPerRow - 1) * interItemSpacing)) / numberOfItemPerRow
            let height = width
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            collectionViewFlowLayout.itemSize = CGSize(width: 110, height: 110)
            collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            collectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
    
    
}

extension CenterViewController: SidePanelViewControllerDelegate {
    func didSelectAnimal(_ animal: Animal) {
        imageView.image = animal.image
        titleLabel.text = animal.title
        creatorLabel.text = animal.creator
        
        delegate?.collapseSidePanels()
        
    }
    
    
}

protocol CenterViewControllerDelegate {
    func toggleLeftPanel()
    func toggleRightPanel()
    func collapseSidePanels()
}

extension CenterViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameString.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as? centerCollectionViewCell
        cell?.centerTextLabel.text = nameString[indexPath.row]
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(nameString[indexPath.row])
        if #available(iOS 13.0, *) {
            if let vc = storyboard?.instantiateViewController(identifier: "mapViewController") as? MapViewController{
                vc.searchingCatagoryName = nameString[indexPath.row]
                navigationController?.pushViewController(vc, animated: true)
            }
        } else {
             if let vc = storyboard?.instantiateViewController(withIdentifier: "mapViewController") as? MapViewController{
                           vc.searchingCatagoryName = nameString[indexPath.row]
                           navigationController?.pushViewController(vc, animated: true)
                       }
        }
        }
        
       // performSegue(withIdentifier: "mapViewShow", sender: self)
  //  }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "mapViewShow" {
//            let destinationVC = segue.destination as! MapViewController
//                destinationVC.addressLabel.text = "Cafe"
//
//        }
//    }
    
}
